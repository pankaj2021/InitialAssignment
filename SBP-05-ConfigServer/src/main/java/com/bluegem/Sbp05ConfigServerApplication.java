package com.bluegem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class Sbp05ConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sbp05ConfigServerApplication.class, args);
	}

}
